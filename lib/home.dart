// Import Packages
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:tflite/tflite.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tesseract_ocr/tesseract_ocr.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _scanning = false;
  bool isBlur = false;
  String _extractText = '';
  int _scanTime = 0;
  // Variables
  bool _loading = true;
  // variable to store loaded image
  File _image;
  // variable to store tflite results
  List _outputBlur, _outputGlare;
  // variable to load image
  final picker = ImagePicker();
  int padSize;

  @override
  void initState() {
    super.initState();
    // load TFLite Model
    loadModel().then((value) {
      setState(() {});
    });
  }

  getOCR(File image) async {
    _scanning = true;
    setState(() {});

    var watch = Stopwatch()..start();
    _extractText = await TesseractOcr.extractText(image.path, language: 'eng');
    _scanTime = watch.elapsedMilliseconds;

    _scanning = false;
    setState(() {});
    print(_extractText);
  }

  // Function to perform TFLite Inference
  classifyImage(File image) async {
    var output = await Tflite.runModelOnImage(
        path: image.path,
        numResults: 2,
        threshold: 0.5,
        imageMean: 127.5,
        imageStd: 127.5,
        asynch: true);
    setState(() {
      // set our global variable equal to local variable

      _outputBlur = output;

      _loading = false;
    });

    print("prediction: $_outputBlur");

    if (_outputBlur[0]['label'] == "0 clear") {
      getOCR(_image);
    } else {
      isBlur = true;
    }
  }

  // Function to Load Model
  loadModel() async {
    // define model path and labels path

    await Tflite.loadModel(
        model: 'assets/blur_model_unquant.tflite',
        labels: 'assets/blur_labels.txt');
  }

  // Function to dispose and clear mmemory once done inferring
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // helps avoid memory leaks
    Tflite.close();
  }

  // Function to pick image - using camera
  getImageFile(ImageSource source) async {
    // load image from source - camera/gallery
    var image = await picker.getImage(source: source);

    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      //ratioX: 1.0,
      //ratioY: 1.0,
      // aspectRatioPresets: [CropAspectRatioPreset.ratio3x2],
      maxWidth: 512,
      maxHeight: 512,
    );

    // check if error laoding image
    if (croppedFile == null) return null;
    setState(() {
      // _image = File(image.path);
      _image = croppedFile;
    });

    // classify image for blur
    classifyImage(_image);
    //for glare
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF101010),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 80),
            // Center(
            //   child: Text(
            //     'TeachableMachines.com CNN',
            //     style: TextStyle(color: Color(0xFFEEDA28), fontSize: 18),
            //     textAlign: TextAlign.center,
            //   ),
            // ),
            // SizedBox(
            //   height: 6,
            // ),
            Center(
              child: Text(
                'Detect Clear and Blur',
                style: TextStyle(
                    color: Color(0xFFE99600),
                    fontWeight: FontWeight.w500,
                    fontSize: 28),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Center(
              child: _loading
                  ? Container(
                      width: 280,
                      child: Column(
                        children: [
                          Image.asset('assets/cat.png'),
                          SizedBox(
                            height: 50,
                          )
                        ],
                      ),
                    )
                  : Container(
                      child: Column(
                        children: [
                          Container(
                            height: 250,
                            child: Image.file(_image),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          _outputBlur != null
                              ? Container(
                                  height: 100,
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          'Blur Model Output',
                                          style: TextStyle(
                                              color: Colors.blueAccent,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          '${_outputBlur[0]['label']}:${_outputBlur[0]['confidence']}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10),
                                        ),
                                        Text(
                                          'OCR Output',
                                          style: TextStyle(
                                              color: Colors.blueAccent,
                                              fontSize: 10),
                                        ),
                                        isBlur
                                            ? Text(
                                                'Image is Blur',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                              )
                                            : Text(
                                                '$_extractText',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                              )
                                      ],
                                    ),
                                  ),
                                )
                              : Container(),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () => getImageFile(ImageSource.camera),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 150,
                      alignment: Alignment.center,
                      padding:
                          EdgeInsets.symmetric(horizontal: 24, vertical: 17),
                      decoration: BoxDecoration(
                        color: Color(0xFFE99600),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Text(
                        'Take a Photo',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () => getImageFile(ImageSource.gallery),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 150,
                      alignment: Alignment.center,
                      padding:
                          EdgeInsets.symmetric(horizontal: 24, vertical: 17),
                      decoration: BoxDecoration(
                        color: Color(0xFFE99600),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Text(
                        'Camera Roll',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
